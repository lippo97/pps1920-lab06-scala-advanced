package u06lab.solution

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class CombinerTest {
  val f: Functions = FunctionsImpl

  @Test def testSum(): Unit = {
    assertEquals(60.1, f.sum(List(10.0,20.0,30.1)))
  }

  @Test def testSumOnEmpty(): Unit = {
    assertEquals(0.0, f.sum(List()))
  }

  @Test def testConcat(): Unit = {
    assertEquals("abc", f.concat(Seq("a","b","c")))
  }

  @Test def testConcatOnEmpty(): Unit = {
    assertEquals("", f.concat(Seq()))
  }

  @Test def testMax(): Unit = {
    assertEquals(3, f.max(List(-10, 3, -5, 0)))
  }

  @Test def testMaxOnEmpty(): Unit = {
    val intMin = Int.MinValue
    assertEquals(intMin, f.max(List()))
  }
}
