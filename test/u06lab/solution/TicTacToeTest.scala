package u06lab.solution

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u06lab.solution.TicTacToe._

class TicTacToeTest {

  @Test
  def testHorizontalWinCondition(): Unit = {
    assertTrue(HorizontalWinCondition.check(List(
      Mark(0, 0, X),
      Mark(0, 1, X),
      Mark(0, 2, X))))
    assertTrue(HorizontalWinCondition.check(List(
      Mark(1, 0, X),
      Mark(1, 1, X),
      Mark(1, 2, X))))
    assertTrue(HorizontalWinCondition.check(List(
      Mark(2, 0, X),
      Mark(2, 1, X),
      Mark(2, 2, X))))
    assertFalse(HorizontalWinCondition.check(List(
      Mark(2, 0, X),
      Mark(2, 1, X),
      Mark(1, 2, X))
    ))
  }

  @Test
  def testHorizontalShouldntWorkOnDifferentPlayers(): Unit = {
    assertFalse(HorizontalWinCondition.check(List(
      Mark(1, 0, X),
      Mark(1, 1, O),
      Mark(1, 2, X))))
  }

  @Test
  def testVerticalWinCondition(): Unit = {
    assertTrue(VerticalWinCondition.check(List(
      Mark(0, 0, X),
      Mark(1, 0, X),
      Mark(2, 0, X))
    ))
    assertTrue(VerticalWinCondition.check(List(
      Mark(0, 1, X),
      Mark(1, 1, X),
      Mark(2, 1, X))
    ))
    assertTrue(VerticalWinCondition.check(List(
      Mark(0, 2, X),
      Mark(1, 2, X),
      Mark(2, 2, X))
    ))
    assertFalse(VerticalWinCondition.check(List(
        Mark(2, 0, X),
        Mark(2, 1, X),
        Mark(1, 2, X))
    ))
  }

  @Test
  def testVerticalShouldntWorkOnDifferentPlayers(): Unit = {
    assertFalse(VerticalWinCondition.check(List(
      Mark(0, 1, O),
      Mark(1, 1, X),
      Mark(2, 1, X))
    ))
  }

  @Test
  def testMajorDiagonalWinCondition: Unit = {
    assertTrue(MajorDiagonalWinCondition.check(List(
      Mark(0, 0, X),
      Mark(1, 1, X),
      Mark(2, 2, X)
    )))
    assertFalse(MajorDiagonalWinCondition.check(List(
      Mark(1, 0, X),
      Mark(1, 1, X),
      Mark(2, 2, X)
    )))
  }

  @Test
  def testMajorDiagonalShouldntWorkOnDifferentPlayers(): Unit = {
    assertFalse(MajorDiagonalWinCondition.check(List(
      Mark(0, 0, X),
      Mark(1, 1, O),
      Mark(2, 2, X)
    )))
  }

  @Test
  def testMinorDiagonalWinCondition: Unit = {
    assertTrue(MinorDiagonalWinCondition.check(List(
      Mark(0, 2, X),
      Mark(1, 1, X),
      Mark(2, 0, X)
    )))
    assertFalse(MinorDiagonalWinCondition.check(List(
      Mark(1, 0, X),
      Mark(1, 1, X),
      Mark(2, 2, X)
    )))
  }

  @Test
  def testMinorDiagonalShouldntWorkOnDifferentPlayers(): Unit = {
    assertFalse(MinorDiagonalWinCondition.check(List(
      Mark(0, 2, X),
      Mark(1, 1, X),
      Mark(2, 0, O)
    )))
  }
}
