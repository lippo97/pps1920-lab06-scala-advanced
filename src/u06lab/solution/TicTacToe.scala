package u06lab.solution

object TicTacToe extends App {
  sealed trait Player{
    def other: Player = this match {case X => O; case _ => X}
    override def toString: String = this match {case X => "X"; case _ => "O"}
  }
  case object X extends Player
  case object O extends Player

  case class Mark(x: Int, y: Int, player: Player)
  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Int, y: Int): Option[Player] = board find { m => m.x == x && m.y == y } map { _.player }

  def placeAnyMark(board: Board, player: Player): Seq[Board] = for (
    x <- 0 to 2;
    y <- 0 to 2;
    if find(board, x, y).isEmpty
  ) yield Mark(x, y, player) :: board

  def computeAnyGame(player: Player, moves: Int): Stream[Game] = moves match {
    case 0 => Stream(List(List()))
    case n => for (
      game <- computeAnyGame(player.other, n - 1);
      next <- placeAnyMark(game.head, player)
    ) yield next :: game
  }

  abstract class WinCondition {
    def check(board: Board): Boolean
  }

  implicit class ComposableWinCondition(wc: WinCondition) {
    def || (other: WinCondition): WinCondition = (board: Board) => wc.check(board) || (other check board)
  }

  abstract class PartitionWinCondition[K] extends WinCondition {
    def partition(board: Board): Map[K, List[Mark]]
    override def check(board: Board): Boolean = partition(board) exists { _._2.length == 3 }
  }

  object VerticalWinCondition extends PartitionWinCondition[(Int, Player)] {
    override def partition(board: Board): Map[(Int, Player), List[Mark]] = board groupBy { m => (m.y, m.player) }
  }

  object HorizontalWinCondition extends PartitionWinCondition[(Int, Player)] {
    override def partition(board: Board): Map[(Int, Player), List[Mark]] = board groupBy { m => (m.x, m.player) }
  }

  object MajorDiagonalWinCondition extends PartitionWinCondition[Player] {
    override def partition(board: Board): Map[Player, List[Mark]] =
      board filter { m => m.x == m.y } groupBy { _.player }
  }

  object MinorDiagonalWinCondition extends PartitionWinCondition[Player] {
    override def partition(board: Board): Map[Player, List[Mark]] =
      board filter { m => m.x == 2-m.y } groupBy { _.player }
  }

  def gameEnd(board: Board): Boolean =
      VerticalWinCondition ||
        HorizontalWinCondition ||
        MajorDiagonalWinCondition ||
        MinorDiagonalWinCondition check board

  def computeAnyGameAndStop(player: Player, moves: Int): Stream[Game] = moves match {
    case 0 => Stream(List(List()))
    case n => for (
      game <- computeAnyGame(player.other, n - 1);
      next <- placeAnyMark(game.head, player);
      end = gameEnd(game.head)
    ) yield if (end) game else next::game
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) { print(" "); if (board == game.head) println()}
    }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0,0,X)),0,0)) // Some(X)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1)) // Some(O)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1)) // None

  // Exercise 2: implement placeAnyMark such that..
//  printBoards(placeAnyMark(List(),X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
//  printBoards(placeAnyMark(List(Mark(0,0,O)),X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
//  computeAnyGame(O, 4) foreach {g => printBoards(g); println()}
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOOz
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
  computeAnyGameAndStop(O, 6) foreach {g => printBoards(g); println()}
}